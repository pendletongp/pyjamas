import pyjd # dummy in pyjs
from pyjamas.JSONService import JSONProxy
from pyjamas.ui.RootPanel import RootPanel
from pyjamas.ui.HTML import HTML
from pyjamas.Timer import Timer

class DataService(JSONProxy):
    methods = ['getRandom']
    def __init__(self):
        JSONProxy.__init__(self, '/ajax_test/service/', DataService.methods)

class AjaxTest:
    def onModuleLoad(self):
        # Setup JSON RPC
        self.remote = DataService()
        
        # Make sure the code is running
        RootPanel().add(HTML("Loaded.", True))
        
        # Test remote
        self.remote.getRandom(self)# must pass handler to get results
        
        # Setup timer to show
        # ajax is working
        remote = self.remote
        handler = self        
        class MyTimer(Timer):
            def run(self):
                remote.getRandom(handler)
        refreshTimer = MyTimer()
        refreshTimer.scheduleRepeating(1000)
        
    def onRemoteResponse(self, response, request_info):
        RootPanel().add(HTML(response, True))
    
    def onRemoteError(self, code, message, request_info):
        RootPanel().add(HTML("Error: %s." % message), True)
        
if __name__ == '__main__':
    # for pyjd, set up a web server and load the HTML from there:
    # this convinces the browser engine that the AJAX will be loaded
    # from the same URI base as the URL, it's all a bit messy...
    pyjd.setup("public/AjaxTest.html")
    app = AjaxTest()
    app.onModuleLoad()
    pyjd.run()