from django.conf.urls.defaults import patterns,url

import views

urlpatterns = patterns('',
    # index
    url(r'^((?P<module>[A-Z_][0-9a-zA-Z_]*)(.(?P<nocache>nocache))?.html)?$',# regex
        views.index,# view
        name='ajax_test-index',# name for reversal
    ),
    # jsonrpc service
    url(r'^service/$',# regex
        views.service,# view
        name='ajax_test-service',# name for reversal
    ),
    # other files
    url(r'(.*)',# regex
        views.redirect_to_static,# view
        name='ajax_test-static',# name for reversal
    ),
)