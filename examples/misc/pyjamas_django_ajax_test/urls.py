from django.conf.urls.defaults import patterns, include
from django.views.generic.simple import redirect_to

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Example:
    # (r'^hangman/', include('hangman.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # (r'^admin/', include(admin.site.urls)),
    
    (r'^$', redirect_to, {'url': '/ajax_test/', 'permanent':False}),
    
    (r'^ajax_test/', include('ajax_test.urls')),
)

from django.conf import settings
urlpatterns += patterns('',
    (r'^'+settings.MEDIA_URL.strip('/')+'/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
)